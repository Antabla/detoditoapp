export class Product {
    constructor(
        public _id: string,
        public name: string,
        public image: string,
        public type: string,
        public sale_price: number,
        public purchase_price: number,
        public description: string,
        public quantity: number,
        public stock: number,
    ) { }
}