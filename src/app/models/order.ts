export class Order {
    constructor(
        public bussines: string,
        public date: string,
        public state: boolean,
        public details: string,
        public shipping_price: number,
        public total_order: number
    ) { }
}