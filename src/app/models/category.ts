export class Category {
    constructor(
        public name: string,
        public image: string
    ) { }
}