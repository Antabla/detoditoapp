export class Bussines {
    constructor(
        public _id: string,
        public name: string,
        public address: string,
        public telephone: string,
        public cellphone: string,
        public email: string,
        public open: boolean,
        public points: number,
        public image: string,
        public category: string
    ) { }

}