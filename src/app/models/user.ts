export class User {
    constructor(
        public _id: string,
        public name: string,
        public surname: string,
        public telephone: string,
        public address: string,
        public username: string,
        public email: string,
        public password: string,
        public favorites: [string],
        public orders: [string],
        public role: string,
        public activate: boolean
    ) { }
}