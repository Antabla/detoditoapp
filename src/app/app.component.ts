import { Component, OnInit } from '@angular/core';
import { User } from './models/user';
import { UserService } from './services/user.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  providers: [UserService]
})
export class AppComponent implements OnInit {
  public title = 'Detodito';
  public user: User;
  public identity;
  public token;

  //para cambiar entre la vista de login y la de registro
  public switchRegister = false;

  //para mostrar informacion en la vista
  public errorMessage;
  public infoMessage;

  constructor(private _userService: UserService) {
    this.user = new User('', '', '', '', '', '', '', '', [''], [''], 'ROLE_USER', false);
  }

  ngOnInit() {
    this.token = this._userService.getToken();
    this.identity = this._userService.getIdentity();
  }

  public login() {
    this.infoMessage = null;
    this.errorMessage = null;

    if (this.isValidate(true)) {
      //Conseguir los datos del usuario identificado
      this._userService.singup(this.user).subscribe(
        response => {
          if (response.error) {
            this.errorMessage = '[' + response.code + '] ' + response.message;
          } else {
            let identity = response.data.user;
            this.identity = identity;

            if (!this.identity._id) {
              alert("El usuario no esta correctamente identificado");
            } else {
              //Guardar en el local storage para tener al usuario en sesion
              localStorage.setItem('identity', JSON.stringify(identity));


              //Conseguir el token para enviarselo a cada peticion http
              this._userService.singup(this.user, true).subscribe(
                response => {
                  if (response.error) {
                    this.errorMessage = '[' + response.code + '] ' + response.message;
                  } else {
                    let token = response.token;
                    this.token = token;

                    if (this.token.length <= 0) {
                      alert('El token no se ha generado correctamente');
                    } else {
                      this.user = new User('', '', '', '', '', '', '', '', [''], [''], 'ROLE_USER', false);
                      localStorage.setItem('token', token);
                    }
                  }
                },
                error => {
                  this.errorMessage = <any>error;
                  if (this.errorMessage != null) {
                    var body = JSON.parse(error._body);
                    this.errorMessage = body.message;
                  }
                });
            }
          }
        },
        error => {
          this.errorMessage = <any>error;
          if (this.errorMessage != null) {
            var body = JSON.parse(error._body);
            this.errorMessage = body.message;
          }
        });
    } else {
      this.errorMessage = "Todos los campos son requeridos";
    }
  }

  public register() {
    this.infoMessage = null;
    this.errorMessage = null;

    if (this.isValidate()) {
      this._userService.create(this.user).subscribe(
        response => {
          if (response.error) {
            this.errorMessage = '[' + response.code + '] ' + response.message;
          } else {
            this.user = new User('', '', '', '', '', '', '', '', [''], [''], 'ROLE_USER', false);
            this.switchRegister = false;
            this.infoMessage = response.message;
          }
        },
        error => {
          this.errorMessage = <any>error;

          if (this.errorMessage != null) {
            var body = JSON.parse(error._body);
            this.errorMessage = body.message;
          }
        });
    } else {
      this.errorMessage = "Todos los campos son requeridos";
    }
  }

  public onlogout() {
    localStorage.clear();
    this.token = null;
    this.identity = null;
  }

  private isValidate(login = false) {
    //validamos que los campos no esten vacios
    let validate = false;
    if (login) {
      if (this.user.username != '' && this.user.password != '') {
        validate = true;
      }
    } else {
      if (this.user.username != '' && this.user.password != '' && this.user.name != '' && this.user.surname != '' && this.user.email != '' && this.user.address != '' && this.user.telephone != '') {
        validate = true;
      }
    }

    return validate;
  }

}
