//Este es el componente general en el cual tendra la navegacion y donde se cargaran los demas componente
import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'detodito-component',
    templateUrl: '../views/detodito.component.html',
})

export class DetoditoComponent implements OnInit {
    @Output() public logout = new EventEmitter<void>();
    public titulo: string;

    //enrutador
    public router;

    constructor() {
        this.router = 'home';
    }

    ngOnInit() {
        console.log('aqui el componente detodito');
    }

    goTo(destination){
        this.router = destination;
    }


}