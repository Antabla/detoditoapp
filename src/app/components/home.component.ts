//Este es el componente general en el cual tendra la navegacion y donde se cargaran los demas componente
import { Component, OnInit} from '@angular/core';

@Component({
    selector: 'home-component',
    templateUrl: '../views/home.component.html',
})

export class HomeComponent implements OnInit {
    public titulo: string;

    //enrutador
    public router;

    constructor() {
        this.router = 'inicio';
    }

    ngOnInit() {
        console.log('aqui el componente home');
    }

    goTo(destination){
        this.router = destination;
    }


}