import { Component, OnInit } from '@angular/core';

import { UserService } from '../services/user.service';
import { User } from '../models/user';

@Component({
    selector: 'user-component',
    templateUrl: '../views/user.component.html',
    providers: [UserService]
})

export class UserComponent implements OnInit {

    public titulo: string;
    public user: User;
    public identity;
    public token;


    //para mostrar informacion en la vista
    public errorMessage;
    public infoMessage;

    constructor(private _userService: UserService) {
    }

    ngOnInit() {
        this.token = this._userService.getToken();
        this.identity = this._userService.getIdentity();
        this.getUserIdentity();
    }

    private getUserIdentity() {
        this.user = new User(
            this.identity._id,
            this.identity.name,
            this.identity.surname,
            this.identity.telephone,
            this.identity.address,
            this.identity.username,
            this.identity.email,
            this.identity.password,
            this.identity.favorites,
            this.identity.orders,
            this.identity.role,
            this.identity.activate);
    }

    public update() {
        this.errorMessage = null;
        this.infoMessage = null;
        this._userService.update(this.user).subscribe(
            //promises buscar
            response => {
                if (response.error) {
                    this.errorMessage = '[' + response.code + '] ' + response.message;
                } else {
                    this.infoMessage = response.message;
                    this._userService.getuser(response.data.user._id).subscribe(
                        response => {
                            if (response.error) {
                                this.errorMessage = '[' + response.code + '] ' + response.message;
                            } else {
                                localStorage.setItem('identity', JSON.stringify(response.data.user));
                                this.identity = this._userService.getIdentity();
                            }
                        },
                        error => {
                            this.errorMessage = <any>error;
                            if (this.errorMessage != null) {
                                var body = JSON.parse(error._body);
                                this.errorMessage = body.message;
                            }
                        }
                    );
                }
            },
            error => {
                this.errorMessage = <any>error;
                if (this.errorMessage != null) {
                    var body = JSON.parse(error._body);
                    this.errorMessage = body.message;
                }
            });
    }

}